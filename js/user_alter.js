(function($) {
  // Override user.js password strength bar
  Drupal.behaviors.password = {
    attach: function(context, settings) {

      $('input.password-field', context).once('password', function () {
        var passwordInput = $(this);
        var innerWrapper = $(this).parent();
        var outerWrapper = $(this).parent().parent();
        var confirmInput = $('input.password-confirm', outerWrapper);


        // Add identifying class to password element parent.
        innerWrapper.addClass('password-parent');
        
        // Add the password confirmation layer.
        $('input.password-confirm', outerWrapper).parent().append('<div class="field-validate"></div>').addClass('confirm-parent');
        var confirmInput = $('input.password-confirm', outerWrapper);
        var confirmResult = $('div.password-confirm', outerWrapper);
        var confirmChild = $('span', confirmResult);
        
        // Check that password and confirmation inputs match.
        var passwordCheckMatch = function () {

          if (confirmInput.val()) {
            var success = passwordInput.val() === confirmInput.val();

            // Show the confirm result.
            confirmResult.css({ visibility: 'visible' });

            // Remove the previous styling if any exists.
            if (this.confirmClass) {
              $(passwordInput).removeClass(this.confirmClass);
              $(confirmInput).removeClass(this.confirmClass);
            }

            // Fill in the success message and set the class accordingly.
            var confirmClass = success ? 'validated' : 'error';
            $(passwordInput).addClass(confirmClass);
            $(confirmInput).addClass(confirmClass);
            //confirmChild.html(translate['confirm' + (success ? 'Success' : 'Failure')]).addClass(confirmClass);
            this.confirmClass = confirmClass;
          }
          else {
            confirmResult.css({ visibility: 'hidden' });
          }
        };
        
        // Monitor keyup and blur events.
        // Blur must be used because a mouse paste does not trigger keyup.
        passwordInput.focus(passwordCheckMatch).blur(passwordCheckMatch);        
        confirmInput.blur(passwordCheckMatch);

      });      
    }  
  };
  
  
  /**
   * Field instance settings screen: force the 'Display on registration form'
   * checkbox checked whenever 'Required' is checked.
   */
  Drupal.behaviors.fieldUserRegistration = {
    attach: function (context, settings) {
      var $checkbox = $('form#field-ui-field-edit-form input#edit-instance-settings-user-register-form');

      if ($checkbox.size()) {
        $('input#edit-instance-required', context).once('user-register-form-checkbox', function () {
          $(this).bind('change', function (e) {
            if ($(this).attr('checked')) {
              $checkbox.attr('checked', true);
            }
          });
        });

      }
    }
  };
})(jQuery);