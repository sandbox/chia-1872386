(function($) {
  
  Drupal.validator = Drupal.validator || {};

  Drupal.behaviors.Validator = {
    attach: function(context, settings) {
      for (var base in settings.validator) {
        if (!$('#' + base + '.validator-processed').length) {
          var element_settings = settings.validator[base];
          if (typeof element_settings.selector == 'undefined') {
            element_settings.selector = '#' + base;
          }
          
          $(element_settings.selector).each(function () {
            element_settings.element = this;
            Drupal.ajax[base] = new Drupal.validator(base, this, element_settings);
          });
          
          $('#' + base).addClass('validator-processed');
        }
      }
    } 
  };

  Drupal.validator = function(base, element, element_settings) {
    var defaults = {
      event: 'blur',
      validator: 'serverside',
      callback: {'success': '', 'error': '', 'complete': ''}
    };

    $.extend(this, defaults, element_settings);

    this.element = element;
    this.element_settings = element_settings;
    var _self = this;
    $(_self.element).bind(element_settings.event, function (event) {
      return _self.eventResponse(this, event);
    });    
  };
  
  Drupal.validator.prototype.eventResponse = function (element, event) {
    // Create a synonym for this to reduce code confusion.
    var _self = this;

    _self.field_value = $(_self.element).val();
    switch (_self.validator) {
      case 'serverside':
        _self.serverside();
        break;
    
      case 'numeric':
        _self.isNumeric();
        break;
    
      case 'nonumeric':
        _self.noNumber();
        break;
    
      case 'email':
        _self.isEmailValid();
        break;
      
      case 'date':
        _self.validateDate();
        break;
    }
    if (typeof _self.callback.complete == 'function') {
      _self.callback.complete.call(this);
    }

  };

  
  Drupal.validator.prototype.serverside = function() {
    var _self = this;
    var field_value = _self.field_value;
    var field_name = _self.field_name;
    $.ajax({
      url: Drupal.settings.basePath + "system/validator",
      data: "field_name=" + field_name + "&field_value=" + field_value ,
      beforeSend: function( xhr ) {
        _self.Validating();
      },
      success: function( response ) {
        if (!response.validated) {
          _self.throwError();
        }
        else {
          _self.Validated();
        }
      }
    });
    
  };
  
  Drupal.validator.prototype.Validating = function() {
    var _self = this;
    $(_self.element).removeClass('validated');
    $(_self.element).removeClass('error');
    $(_self.element).addClass('validating');
  };
  
  Drupal.validator.prototype.Validated = function() {
    var _self = this;
    $(_self.element).removeClass('validating');
    $(_self.element).removeClass('error'); 
    $(_self.element).addClass('validated');   
  };
  
  Drupal.validator.prototype.throwError = function() {
    var _self = this;
    $(_self.element).removeClass('validating');
    $(_self.element).removeClass('validated');
    $(_self.element).addClass('error');
  };
  
  Drupal.validator.prototype.isNumeric = function() {
    var _self = this;
    var field_value = _self.field_value;
    if (!field_value || (!isNaN(parseFloat(field_value)) && isFinite(field_value))) {
      _self.Validated();
    }
    else {
      _self.throwError();
    }
  };
  
  Drupal.validator.prototype.noNumber = function() {
    var _self = this;
    var field_value = _self.field_value;
    var regex = new RegExp(/\d/g);
    if (regex.test(field_value)) {
      _self.throwError();
    }
    else {
      _self.Validated();
    }    
  };
  
  Drupal.validator.prototype.isEmailValid = function() {
    var _self = this;
    var field_value = _self.field_value;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (pattern.test(field_value)) {
      _self.Validated();
    }
    else {
      _self.throwError();
    }
  };
  
  Drupal.validator.prototype.validateDate = function() {
    var _self = this;
    var $parent = $(_self.element).parents('.form-type-date-select:eq(0)');
    var date = $('select[name="' + _self.field_name + '[und][0][value][day]"]', $parent).val();
    var month = $('select[name="' + _self.field_name + '[und][0][value][month]"]', $parent).val();
    var year = $('select[name="' + _self.field_name + '[und][0][value][year]"]', $parent).val();
    var newDate = new Date(year, month - 1, date);
    if (newDate > _self.opts.greaterThan) {
      _self.throwError();
    }
    else {
      _self.Validated();
    }
  };
  
  Drupal.validator.prototype.Length = function(maxlength) {
    var _self = this;
    return _self.field_value.length;
  };
})(jQuery);