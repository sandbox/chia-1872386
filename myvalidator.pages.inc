<?php 

/**
 * Ajax field validate
 */
function myvalidator_serverside_callback($field_name = NULL) {
  if (empty($field_name)) {
    $field_name = filter_xss($_REQUEST['field_name']);
  }

  $field_value = $_REQUEST['field_value'];
  $error = NULL;
  switch ($field_name) {
    case 'mail':
      if ($error = user_validate_mail($field_value)) {
      }
      elseif (db_select('users')->fields('users', array('uid'))->condition('mail', db_like($field_value), 'LIKE')->range(0, 1)->execute()->fetchField()) {
        $error = t('The e-mail address %email is already registered. <a href="@password">Have you forgotten your password?</a>', array('%email' => $field_value, '@password' => url('yelmo/password/nojs')));
      }

      break;

    default:
      $error = module_invoke_all('myvalidator_validation', $field_name, $field_value);
      break;

  }

  if ($error) {
    $output = array('validated' => false, 'error' => $error);
    drupal_json_output($output);
  }
  else {
    $output = array('validated' => true);
    drupal_json_output($output);
  }
}
